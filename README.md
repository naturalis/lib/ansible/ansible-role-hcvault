ansible-role-hcvault
=========

Imports secrets from the central Hashicorp Vault. See for documentation [here](https://docs.infra.naturalis.io/software/vault/)


Role Variables
--------------

| Name           | Default value | Description                        | Optional |
| -------------- | ------------- | -----------------------------------|----------|
| vault_path | EMPTY | gitlab path used as Vault path i.e.:  `/gii/mattermost` | Required
| vault_token | "{{ lookup( 'env', 'VAULT_TOKEN') }}"  | Token is taken from ENV | Required in VAULT_TOKEN env 
| vault_shared_secrets_path | a path for retrieving shared credentials | Vault path for shared secrets | Optional 
| vault_custom_secrets_path | EMPTY  | path (relative to kv2/data) of any secret in Vault that user can read | Optional

Role outputs
------------

All of the default options below are always retrieved by this role (if available). The optional outputs are retrieved when a specific variable is set

Default
=======

`secrets_all`: all secrets in `vault_path\ansible-application\group_vars\all`
`secrets_group`: all secrets sorted by group name in `vault_path\ansible-application\group_vars\groups\` (e.g. secrets_group.development.secretname = secret_value)
`vault_tenant_secrets`: All secrets sorted by services (see `_tenant_secrets`) from `vault_path\tenant` (e.g. vault_tenant_secrets.letsencrypt)

Optional
========
`shared_secrets`: All secrets from `vault_shared_secrets_path`
`vault_custom_secrets`: All secrets from `vault_custom_secrets_path`

Dependencies
------------
You might `pip(3) install hvac`

Example Playbook
----------------

Use this in your playbook:
```
- name: Include HashiCorpVault secrets
  hosts: all
  tags: always
  gather_facts: false
  tasks:
    - name: Get HC Vault secrets
      include_role:
        name: hc-vault
        apply:
          tags: hcv
```

And in requirements.yml

```
  - src: https://gitlab.com/naturalis/lib/ansible/ansible-role-hcvault
    scm: git
    version: 0.1.4
    name: hc-vault
collections:
  - name: community.hashi_vault
    version: 6.2.0

```

License
-------

BSD

